import {
  Hook,
  Promiseable,
  CallerFn,
  HookIteratorContext,
  FunctionArguments,
  forgeArgumentNames,
} from './Hook';
import {
  createActionIteratorSeries,
  ActionIteratorSeries,
} from './factory/actionSeries';

export * from './Hook';

export class HookActionSeries<Caller extends CallerFn = CallerFn> extends Hook<Caller, Promise<void>> {

  static template<Caller extends CallerFn = CallerFn>(caller: Caller): HookActionSeries<(...args: FunctionArguments<Caller>) => Promiseable<ReturnType<Caller>>> {
    return new HookActionSeries(forgeArgumentNames(caller));
  }

  addAction = this.bind;
  removeAction = this.unbind;

  protected createIteration(): ActionIteratorSeries<Caller> {
    return createActionIteratorSeries<Caller>(this);
  }

  do(...args: FunctionArguments<Caller>) {
    return this.iterate().call(...args);
  }

  doPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>) {
    return this.iterate(context).call(...args);
  }

}