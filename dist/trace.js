"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.symbol");

require("core-js/modules/es.symbol.description");

require("core-js/modules/es.array.concat");

require("core-js/modules/es.array.iterator");

require("core-js/modules/es.object.to-string");

require("core-js/modules/web.dom-collections.iterator");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.trace = trace;

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _Hook = require("./Hook");

function getStack() {
  // Save original Error.prepareStackTrace
  var origPrepareStackTrace = Error.prepareStackTrace; // Override with function that just returns `stack`

  Error.prepareStackTrace = function (_, stack) {
    return stack;
  }; // Create a new `Error`, which automatically gets `stack`


  var err = new Error(); // Evaluate `err.stack`, which calls our new `Error.prepareStackTrace`

  var stack = err.stack; // Restore original `Error.prepareStackTrace`

  Error.prepareStackTrace = origPrepareStackTrace;
  stack.shift(); // getStack

  return stack;
}

var supportStack = true;

function formatStack() {
  var _context;

  var start = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var many = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var stack = (0, _slice.default)(_context = getStack()).call(_context, 4 + start, 4 + start + many);
  return "\n  ".concat((0, _map.default)(stack).call(stack, function (callSite) {
    var functionName = callSite.getFunctionName();
    var fileNameWithLine = "".concat(callSite.getFileName(), ":").concat(callSite.getLineNumber(), ":").concat(callSite.getColumnNumber());
    return "at ".concat(functionName ? "".concat(functionName, " (").concat(fileNameWithLine, ")") : "".concat(fileNameWithLine));
  }).join('\n  '));
}

function inspectValue(value) {
  if ('process' in global) {
    var util = require('util');

    return util.inspect(value);
  }

  return (0, _stringify.default)(value);
}

function printInspect(message) {
  var stack = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var start = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var many = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;

  if (stack && supportStack) {
    // tslint:disable-next-line no-parameter-reassignment
    message += formatStack(1 + start, many);
  }

  console.log(message);
}

function trace(hook, namespace) {
  var stack = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var manyStack = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;
  var iteratorId = -1;
  var printNamespace = namespace ? "".concat(namespace, ": ") : '';
  var unsubs = [];
  var unsubscribed = false;
  unsubs.push(hook.intercept({
    bind(plugin) {
      var priority = "".concat(plugin.priority == null ? "default ".concat(hook.defaultPriority) : plugin.priority);
      var message = "".concat(printNamespace, "Plugin ").concat((0, _Hook.serializePluginName)(plugin), " was binded with (mode: ").concat(plugin.mode, ", ").concat(plugin.order && plugin.order.length ? "order: ".concat(inspectValue(plugin.order), ", priorityFallback: ").concat(priority) : "priority: ".concat(priority), ")");
      printInspect(message, stack, 0, manyStack);
      unsubs.push(plugin.intercept({
        enable(state) {
          printInspect("".concat(printNamespace, "Plugin ").concat((0, _Hook.serializePluginName)(plugin), " was ").concat(state ? 'enabled' : 'disabled'), stack, 0, manyStack);
        }

      }));
    },

    unbind(plugin) {
      printInspect("".concat(printNamespace, "Plugin ").concat((0, _Hook.serializePluginName)(plugin), " was unbind"), stack, 0, manyStack);
    },

    iterate(iterator) {
      iteratorId += 1;
      var id = iteratorId;
      printInspect("".concat(printNamespace, "Iterator#").concat(id, " created"), stack, 1, manyStack);
      unsubs.push(iterator.intercept({
        call() {
          var argPrint = [];

          for (var i = 0; i < arguments.length; i += 1) {
            argPrint.push("".concat(hook.argVars && hook.argVars[i] ? hook.argVars[i] : "arg_".concat(i), ": ").concat(inspectValue(i < 0 || arguments.length <= i ? undefined : arguments[i])));
          }

          printInspect("".concat(printNamespace, "Iterator#").concat(id, " called with (").concat(argPrint.join(', '), ")"), stack, 1, manyStack);
        },

        bail(error) {
          printInspect("".concat(printNamespace, "Iterator#").concat(id, " bailed with").concat(error ? '' : 'out', " error"), stack, 0, manyStack);
        },

        enable(name, state) {
          printInspect("".concat(printNamespace, "Iterator#").concat(id, " ").concat(state ? 'enabled' : 'disabled', " \"").concat(name, "\""), stack, 1, manyStack);
        },

        goto(name) {
          printInspect("".concat(printNamespace, "Iterator#").concat(id, " skipped to \"").concat(name, "\""), stack, 0, manyStack);
        }

      }));
    }

  }));
  return function () {
    if (!unsubscribed) {
      unsubscribed = true;
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = unsubs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var unsubscribe = _step.value;
          unsubscribe();
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return true;
    }

    return false;
  };
}
//# sourceMappingURL=trace.js.map