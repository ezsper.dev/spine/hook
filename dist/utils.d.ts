export declare function makeArray<T>(input: T): T extends any[] ? T : T[];
export declare function sortNumbers(a: string | number, b: string | number): number;
