import * as symbols from './symbols';
declare const $state$: typeof symbols.$state$;
export declare type Promiseable<T> = T extends Promise<infer U> ? (U | T) : (T | Promise<T>);
export declare type FunctionArguments<Func extends CallerFn> = Func extends (...args: infer Args) => any ? Args : never;
export declare type InterceptorEvents = {
    [name: string]: Function;
};
export declare class Interceptor<Trigger extends InterceptorEvents> {
    private interceptors?;
    readonly trigger: Partial<Trigger>;
    constructor();
    intercept: (interceptor: Partial<Trigger>) => () => boolean;
}
export declare type HookIteratorContext = Record<string, any>;
export declare type HookIteratorEvents<Caller extends (...args: any[]) => any, IterateCallResult = any> = {
    call(...args: FunctionArguments<Caller>): void;
    bail(error?: Error): void;
    goto(name: HookPluginName): void;
    enable(name: HookPluginName, state: boolean): void;
};
export interface HookIterator<Caller extends CallerFn, Result = void> {
    /**
     * Hook owner
     */
    readonly hook: Hook<Caller, Result>;
    /**
     * Passing context
     */
    readonly passingContext: boolean;
    /**
     * Iterator context
     */
    readonly context: HookIteratorContext;
    /**
     * Iterator interceptor
     */
    intercept(interceptor: Partial<HookIteratorEvents<Caller, Result>>): () => boolean;
    /**
     * Iterator interceptor
     */
    readonly interceptor: Interceptor<HookIteratorEvents<Caller, Result>>;
    /**
     * Execute iterator
     * @param args
     */
    call(...args: FunctionArguments<Caller>): Result;
    /**
     * Bail iteration
     * @param error
     */
    bail(error?: Error): this;
    /**
     * Find plugin index by name
     * @param name
     */
    findIndex(name: HookPluginName): number;
    /**
     * Get iterator plugin state
     */
    getState(): ReadonlyArray<HookListener<Caller>>;
    /**
     * Check if plugin exists by name
     * @param name
     */
    has(name: HookPluginName): boolean;
    /**
     * Go to plugin by name
     * @param name
     * @result Return true if plugin is found
     */
    goto(name: HookPluginName): boolean;
    /**
     * Check if plugin is enabled by name
     * @param name
     * @result Return the boolean status or null if plugin is not found
     */
    status(name: HookPluginName): boolean | null;
    /**
     * Enable a plugin by name
     * @param name
     * @param enable Enable status
     */
    enable(name: HookPluginName, enable?: boolean): boolean;
    /**
     * Disable a plugin by name
     * @param name
     */
    disable(name: HookPluginName): boolean;
}
export declare function forgeArgumentNames<Caller extends CallerFn>(caller: Caller): ArgumentNames<Caller> | undefined;
export declare type CallerFn<Params extends [...any[]] = [...any[]], Result = any> = (...args: Params) => Result;
export declare type CallerWithIterator<Caller extends CallerFn> = (iterator: HookIterator<Caller>, ...args: FunctionArguments<Caller>) => ReturnType<Caller>;
export declare type CallerWithContext<Caller extends CallerFn> = (context: HookIteratorContext, ...args: FunctionArguments<Caller>) => ReturnType<Caller>;
export declare type ArgumentNames<Caller extends CallerFn> = FunctionArguments<Caller> extends [any, any, any, any] ? [string, string, string, string] : FunctionArguments<Caller> extends [any, any, any] ? [string, string, string] : FunctionArguments<Caller> extends [any, any] ? [string, string] : FunctionArguments<Caller> extends [any] ? [string] : FunctionArguments<Caller> extends [] ? [] : string[];
export declare type HookEvents<Caller extends (...args: any[]) => any, IterateCallResult = any> = {
    /**
     * When a new plugin is binded
     * @param plugin
     */
    bind(plugin: HookListener<Caller>): void;
    /**
     * When a plugin is unbinded
     * @param plugin
     */
    unbind(plugin: HookListener<Caller>): void;
    /**
     * When hook iteration is created
     * @param iterator
     * @param args
     */
    iterate(iterator: HookIterator<Caller, IterateCallResult>): void;
    /**
     * When hook is called
     * @param iterator
     * @param args
     */
    call(iterator: HookIterator<Caller, IterateCallResult>, ...args: FunctionArguments<Caller>): void;
};
export declare function serializePluginName(name: HookListener | HookPluginName): string;
export declare abstract class Hook<Caller extends CallerFn = CallerFn, IterateCallResult = any> {
    readonly argVars?: ArgumentNames<Caller> | undefined;
    readonly defaultPriority: number;
    private [$state$];
    constructor(argVars?: ArgumentNames<Caller> | undefined, defaultPriority?: number);
    intercept(interceptor: Partial<HookEvents<Caller, IterateCallResult>>): () => boolean;
    readonly interceptor: Interceptor<HookEvents<Caller, IterateCallResult>>;
    protected abstract createIteration(): (this: HookIterator<Caller, IterateCallResult>, ...args: FunctionArguments<Caller>) => IterateCallResult;
    has(plugin: HookListener<Caller>): boolean;
    has(name: HookPluginName): boolean;
    get(name: HookPluginName): HookListener<Caller, CallerMode, Caller | CallerWithContext<Caller> | CallerWithIterator<Caller>> | null;
    get size(): number;
    bind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default', caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<ListenerCaller extends Caller>(name: HookPluginName, iterate: null, caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller, priority?: number): HookListener<Caller, 'iterator', ListenerCaller>;
    bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller, priority?: number): HookListener<Caller, 'iterator', ListenerCaller>;
    bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller, priority?: number): HookListener<Caller, 'context', ListenerCaller>;
    bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller, priority?: number): HookListener<Caller, 'context', ListenerCaller>;
    bind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller, priority?: number): HookListener<Caller, Mode, ListenerCaller>;
    bind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<ListenerCaller extends Caller>(name: HookPluginName, iterate: null, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'iterator', ListenerCaller>;
    bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'iterator', ListenerCaller>;
    bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'context', ListenerCaller>;
    bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'context', ListenerCaller>;
    bind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, Mode, ListenerCaller>;
    bind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>;
    bind<P extends HookListener<Caller>>(plugin: P): P;
    unbind(name: HookPluginName): HookListener<Caller> | null;
    unbind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller): HookListener<Caller, 'default', ListenerCaller> | null;
    unbind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default' | null, caller: ListenerCaller): HookListener<Caller, 'default', ListenerCaller> | null;
    unbind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller): HookListener<Caller, 'iterator', ListenerCaller> | null;
    unbind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller): HookListener<Caller, 'iterator', ListenerCaller> | null;
    unbind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller): HookListener<Caller, 'context', ListenerCaller> | null;
    unbind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller): HookListener<Caller, 'context', ListenerCaller> | null;
    unbind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller): HookListener<Caller, Mode, ListenerCaller> | null;
    unbind<P extends HookListener<Caller>>(plugin: P): P | null;
    plugins(): HookListener<Caller, CallerMode, Caller | CallerWithContext<Caller> | CallerWithIterator<Caller>>[];
    iterate(context?: HookIteratorContext): HookIterator<Caller, IterateCallResult>;
}
export declare type CallerMode = 'context' | 'iterator' | 'default';
export declare type CallerOrder = {
    before: HookPluginName;
} | {
    after: HookPluginName;
};
export declare type CallerWithMode<Mode extends CallerMode, Caller extends CallerFn> = Mode extends 'iterator' ? CallerWithIterator<Caller> : (Mode extends 'context' ? CallerWithContext<Caller> : Caller);
export declare type HookListenerEvents<Caller extends CallerFn, Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>> = {
    bind(hook: Hook): void;
    unbind(hook: Hook): void;
    enable(state: boolean): void;
};
export declare type HookPluginName = string | HookPlugin | [string | HookPlugin] | [undefined | string | HookPlugin, string];
export declare class HookListener<Caller extends CallerFn = CallerFn, Mode extends CallerMode = CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller> = CallerWithMode<Mode, Caller>> {
    readonly hook: Hook<Caller>;
    readonly mode: Mode;
    readonly caller: ListenerCaller;
    readonly order?: CallerOrder[] | undefined;
    readonly priority?: number | undefined;
    readonly disabled: boolean;
    readonly name: string;
    readonly contextValue?: HookPlugin | string;
    readonly context?: string;
    constructor(name: HookPluginName, hook: Hook<Caller>, mode: Mode, caller: ListenerCaller, order?: CallerOrder[] | undefined, priority?: number | undefined);
    intercept(interceptor: Partial<HookListenerEvents<Caller, Mode, ListenerCaller>>): () => boolean;
    readonly interceptor: Interceptor<HookListenerEvents<Caller, Mode, ListenerCaller>>;
    callerLength: number | undefined;
    get binded(): boolean;
    bind(): this;
    unbind(): this | null;
    /**
     * Usable when resting arguments
     * @param length
     */
    args(length: number): this;
    enable(state?: boolean): this;
    disable(): this;
    toString(): string;
}
export interface HookPlugin {
    hookContext: string;
}
export {};
