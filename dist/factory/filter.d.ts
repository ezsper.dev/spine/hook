import { Hook, Promiseable, FunctionArguments, HookIterator } from '../Hook';
export declare type FilterCaller<T> = (...args: [T, ...any[]]) => Promiseable<T>;
export declare type FilterIterator<T, Caller extends FilterCaller<T>> = (this: HookIterator<Caller, Promise<T>>, ...args: FunctionArguments<Caller>) => Promise<T>;
export declare function createFilterIterator<T, Caller extends FilterCaller<any>>(hook: Hook<Caller, Promise<T>>): FilterIterator<T, Caller>;
