"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFilterSyncIterator = createFilterSyncIterator;

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var symbols = _interopRequireWildcard(require("../symbols"));

var _shared = require("./shared");

// maintain, direct symbol importation slow down iteration due to exports.$state$
var $state$ = symbols.$state$;

function createFilterSyncIterator(hook) {
  var plugins = hook.plugins();

  var iterate = function iterate() {
    var _context;

    return "try {\n".concat((0, _shared.iteratorHead)(hook), "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
      return plugin.disabled ? "_state.disabled".concat(i, " = true;\n") : '';
    }).join(''), "\n").concat(hook.argVars ? (0, _map.default)(_context = hook.argVars).call(_context, function (name, index) {
      return "var ".concat(name, " = arguments[").concat(index, "];");
    }).join('\n') : '', "\n").concat(hook.argVars ? '' : 'var _value = arguments[0];', "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
      return "if (_state.startIndex <= ".concat(i, " && ").concat(i > 0 ? "!_state.bailed && " : '', "!_state.disabled").concat(i, ") {\n  ").concat(hook.argVars ? hook.argVars[0] : '_value', " = _caller").concat(i, "(").concat(plugin.mode !== 'default' ? plugin.mode === 'iterator' ? '_iterator' : '_context' : '').concat(plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : '').concat((0, _shared.mapArguments)(hook.argVars, (plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0), function (index) {
        return index === 0 && !hook.argVars ? '_value' : undefined;
      }), ");\n}");
    }).join('\n  '), "\n").concat(_shared.iteratorErrorThrown, "\n").concat(_shared.iteratorDestroy, "\nif (_state.return) {\n  var _arguments = Array.from(arguments);\n  _arguments[0] = ").concat(hook.argVars ? hook.argVars[0] : '_value', ";\n  return _state.return.apply(this, _arguments);\n}\nreturn ").concat(hook.argVars ? hook.argVars[0] : '_value', ";\n} catch (error) {\n").concat(_shared.iteratorDestroy, "\n  throw error;\n}");
  };

  return Function("".concat(_shared.iteratorOuterHead, "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
    return "var _plugin".concat(i, " = _self.plugins[").concat(i, "];");
  }).join('\n'), "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
    return "var _caller".concat(i, " = _plugin").concat(i, ".caller;");
  }).join('\n'), "\nreturn function _iterate() {\n").concat(iterate(), "\n}")).call({
    $state$,
    plugins
  });
}
/*import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import { mapArguments } from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type FilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T> = (this: HookIterator<Caller>, ...args: FunctionArguments<Caller>) => T;

export function createFilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T>(hook: Hook<Caller>): FilterSyncIterator<T, Caller> {
  const state = hook[$state$];
  return Function(`"use strict";
var _self = this;
var $state$ = _self.$state$;
${state.pluginOrder.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${state.pluginOrder.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
function _iterate() {
  var _iterator = this;
  var _state = this[$state$];
  var _value = arguments[0];
  ${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
  ${state.pluginOrder.map((plugin, i) => `if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
    _value = _caller${i}(${plugin.iterate ? '_iterator' : ''}${plugin.iterate && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.iterate ? -1 : 0 ))});
  }`).join('\n  ')}
  _state.destroyed = true;
  if (_state.errorThrown) {
    throw _state.errorThrown;
  }
  return _value;
}
return _iterate;`).call({ $state$, plugins: state.pluginOrder });
}
*/
//# sourceMappingURL=filterSync.js.map