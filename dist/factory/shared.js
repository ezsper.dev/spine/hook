"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

require("core-js/modules/es.string.split");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapArguments = mapArguments;
exports.mapArgumentsPrefix = mapArgumentsPrefix;
exports.iteratorErrorThrown = exports.iteratorDestroy = exports.iteratorHead = exports.iteratorOuterHead = void 0;

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var iteratorOuterHead = "\"use strict\";\nvar _self = this;\nvar $state$ = _self.$state$;";
exports.iteratorOuterHead = iteratorOuterHead;

var iteratorHead = function iteratorHead(hook) {
  return "var _iterator = this;\nvar _hook = _iterator.hook;\nvar _interceptorHook = _hook.interceptor;\nvar _state = _iterator[$state$];\nvar _context = _iterator.context;\nvar _interceptor = _iterator.interceptor;\nvar _triggerCall = _interceptor.trigger.call;\nif (_triggerCall && !_state.triggerGoto) {\n  _triggerCall.apply(null, arguments);\n}\nvar _triggerHookCall = _interceptorHook.trigger.call;\nif (_triggerHookCall && !_state.triggerGoto) {\n  _triggerHookCall.apply(null, [_iterator].concat(Array.from(arguments)));\n}\n";
};

exports.iteratorHead = iteratorHead;
var iteratorDestroy = "\n_state.destroyed = true;\n";
exports.iteratorDestroy = iteratorDestroy;
var iteratorErrorThrown = "if (_state.errorThrown) {\n  throw _state.errorThrown;\n}";
exports.iteratorErrorThrown = iteratorErrorThrown;

function mapArguments(names, length) {
  var mapArgument = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function (index) {
    return undefined;
  };
  return mapArgumentsPrefix('', names, length, mapArgument);
}

function mapArgumentsPrefix(prefix, names, length) {
  var _context;

  var mapArgument = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (index) {
    return undefined;
  };
  return (0, _map.default)(_context = Array(length).join('.').split('.')).call(_context, function (empty, index) {
    return mapArgument(index, names ? names[index] : undefined) || (names && names[index] ? names[index] : "".concat(prefix, "arguments[").concat(index, "]"));
  }).join(', ');
}
//# sourceMappingURL=shared.js.map