import { Hook, FunctionArguments, HookIterator } from '../Hook';
export declare type ActionSyncIterator<Caller extends (...args: any[]) => any> = (this: HookIterator<Caller, void>, ...args: FunctionArguments<Caller>) => void;
export declare function createActionSyncIterator<Caller extends (...args: any[]) => any>(hook: Hook<Caller, any>): ActionSyncIterator<Caller>;
