"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createActionIteratorSeries = createActionIteratorSeries;

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var symbols = _interopRequireWildcard(require("../symbols"));

var _shared = require("./shared");

// maintain, direct symbol importation slow down iteration due to exports.$state$
var $state$ = symbols.$state$;

function createActionIteratorSeries(hook) {
  var plugins = hook.plugins();

  var iterate = function iterate() {
    var _context;

    return "".concat((0, _shared.iteratorHead)(hook), "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
      return plugin.disabled ? "_state.disabled".concat(i, " = true;\n") : '';
    }).join(''), "\nvar _promise = Promise.resolve();\nvar _arguments = arguments;\n").concat(hook.argVars ? (0, _map.default)(_context = hook.argVars).call(_context, function (name, index) {
      return "var ".concat(name, " = arguments[").concat(index, "];");
    }).join('\n') : '', "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
      return "\n    _promise = _promise.then(function () {\n      var _innerPromise;\n      if (_state.startIndex <= ".concat(i, " && ").concat(i > 0 ? "!_state.bailed && " : '', "!_state.disabled").concat(i, ") {\n        _innerPromise = _caller").concat(i, "(").concat(plugin.mode !== 'default' ? plugin.mode === 'iterator' ? '_iterator' : '_context' : '').concat(plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : '').concat((0, _shared.mapArgumentsPrefix)('_', hook.argVars, (plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0)), ");\n        return _innerPromise;\n      }\n    });\n");
    }).join('\n  '), "\nreturn _promise\n  .then(function() {\n    ").concat(_shared.iteratorErrorThrown, "\n    ").concat(_shared.iteratorDestroy, "\n    if (_state.return) {\n      return _state.return.apply(this, arguments);\n    }\n  })\n  .catch(function (error) {\n    ").concat(_shared.iteratorDestroy, "\n    throw error;\n  });");
  };

  return Function("".concat(_shared.iteratorOuterHead, "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
    return "var _plugin".concat(i, " = _self.plugins[").concat(i, "];");
  }).join('\n'), "\n").concat((0, _map.default)(plugins).call(plugins, function (plugin, i) {
    return "var _caller".concat(i, " = _plugin").concat(i, ".caller;");
  }).join('\n'), "\nreturn function _iterate() {\n").concat(iterate(), "\n}")).call({
    $state$,
    plugins
  });
}
//# sourceMappingURL=actionSeries.js.map