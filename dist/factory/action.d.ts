import { Hook, FunctionArguments, HookIterator } from '../Hook';
export declare type ActionIterator<Caller extends (...args: any[]) => any> = (this: HookIterator<Caller, Promise<void>>, ...args: FunctionArguments<Caller>) => Promise<void>;
export declare function createActionIterator<Caller extends (...args: any[]) => any>(hook: Hook<Caller, any>): ActionIterator<Caller>;
