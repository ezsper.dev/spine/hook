import { Hook, FunctionArguments, HookIterator } from '../Hook';
export declare type FilterSyncCaller<T> = (...args: [T, ...any[]]) => T;
export declare type FilterSyncIterator<T, Caller extends FilterSyncCaller<T>> = (this: HookIterator<Caller, T>, ...args: FunctionArguments<Caller>) => T;
export declare function createFilterSyncIterator<T, Caller extends FilterSyncCaller<T>>(hook: Hook<Caller, T>): FilterSyncIterator<T, Caller>;
