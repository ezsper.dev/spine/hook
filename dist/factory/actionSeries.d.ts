import { Hook, FunctionArguments, HookIterator } from '../Hook';
export declare type ActionIteratorSeries<Caller extends (...args: any[]) => any> = (this: HookIterator<Caller, Promise<void>>, ...args: FunctionArguments<Caller>) => Promise<void>;
export declare function createActionIteratorSeries<Caller extends (...args: any[]) => any>(hook: Hook<Caller, any>): ActionIteratorSeries<Caller>;
