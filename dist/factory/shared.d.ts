import { Hook } from '../Hook';
export declare const iteratorOuterHead = "\"use strict\";\nvar _self = this;\nvar $state$ = _self.$state$;";
export declare const iteratorHead: (hook: Hook<import("../Hook").CallerFn<any[], any>, any>) => string;
export declare const iteratorDestroy = "\n_state.destroyed = true;\n";
export declare const iteratorErrorThrown = "if (_state.errorThrown) {\n  throw _state.errorThrown;\n}";
export declare function mapArguments(names: string[] | undefined, length: number, mapArgument?: (index: number, name: string | undefined) => string | undefined | null): string;
export declare function mapArgumentsPrefix(prefix: string, names: string[] | undefined, length: number, mapArgument?: (index: number, name: string | undefined) => string | undefined | null): string;
