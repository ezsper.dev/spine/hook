import { Hook, CallerFn, FunctionArguments, HookIteratorContext } from './Hook';
import { FilterSyncIterator } from './factory/filterSync';
export * from './Hook';
export declare type FilterSyncCallerFn<T> = CallerFn<[T, ...any[]], T>;
export declare class HookFilterSync<T = any, Caller extends FilterSyncCallerFn<T> = FilterSyncCallerFn<T>> extends Hook<Caller, T> {
    static template<T, Caller extends FilterSyncCallerFn<T>>(caller: FilterSyncCallerFn<T> & Caller): HookFilterSync<T, Caller>;
    addFilter: {
        <ListenerCaller extends Caller>(name: import("./Hook").HookPluginName, mode: "default", caller: ListenerCaller, priority?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller>;
        <ListenerCaller_1 extends Caller>(name: import("./Hook").HookPluginName, iterate: null, caller: ListenerCaller_1, priority?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller_1>;
        <ListenerCaller_2 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, mode: "iterator", caller: ListenerCaller_2, priority?: number | undefined): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_2>;
        <ListenerCaller_3 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, iterate: true, caller: ListenerCaller_3, priority?: number | undefined): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_3>;
        <ListenerCaller_4 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, mode: "context", caller: ListenerCaller_4, priority?: number | undefined): import("./Hook").HookListener<Caller, "context", ListenerCaller_4>;
        <ListenerCaller_5 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, iterate: false, caller: ListenerCaller_5, priority?: number | undefined): import("./Hook").HookListener<Caller, "context", ListenerCaller_5>;
        <Mode extends import("./Hook").CallerMode, ListenerCaller_6 extends import("./Hook").CallerWithMode<Mode, Caller>>(name: import("./Hook").HookPluginName, mode: Mode, caller: ListenerCaller_6, priority?: number | undefined): import("./Hook").HookListener<Caller, Mode, ListenerCaller_6>;
        <ListenerCaller_7 extends Caller>(name: import("./Hook").HookPluginName, caller: ListenerCaller_7, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller_7>;
        <ListenerCaller_8 extends Caller>(name: import("./Hook").HookPluginName, mode: "default", caller: ListenerCaller_8, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller_8>;
        <ListenerCaller_9 extends Caller>(name: import("./Hook").HookPluginName, iterate: null, caller: ListenerCaller_9, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller_9>;
        <ListenerCaller_10 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, mode: "iterator", caller: ListenerCaller_10, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_10>;
        <ListenerCaller_11 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, iterate: true, caller: ListenerCaller_11, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_11>;
        <ListenerCaller_12 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, mode: "context", caller: ListenerCaller_12, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "context", ListenerCaller_12>;
        <ListenerCaller_13 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, iterate: false, caller: ListenerCaller_13, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, "context", ListenerCaller_13>;
        <Mode_1 extends import("./Hook").CallerMode, ListenerCaller_14 extends import("./Hook").CallerWithMode<Mode_1, Caller>>(name: import("./Hook").HookPluginName, mode: Mode_1, caller: ListenerCaller_14, order: {
            before: import("./Hook").HookPluginName;
        } | {
            after: import("./Hook").HookPluginName;
        } | import("./Hook").CallerOrder[], priorityFallback?: number | undefined): import("./Hook").HookListener<Caller, Mode_1, ListenerCaller_14>;
        <ListenerCaller_15 extends Caller>(name: import("./Hook").HookPluginName, caller: ListenerCaller_15, priority?: number | undefined): import("./Hook").HookListener<Caller, "default", ListenerCaller_15>;
        <P extends import("./Hook").HookListener<Caller, import("./Hook").CallerMode, Caller | import("./Hook").CallerWithContext<Caller> | import("./Hook").CallerWithIterator<Caller>>>(plugin: P): P;
    };
    removeFilter: {
        (name: import("./Hook").HookPluginName): import("./Hook").HookListener<Caller, import("./Hook").CallerMode, Caller | import("./Hook").CallerWithContext<Caller> | import("./Hook").CallerWithIterator<Caller>> | null;
        <ListenerCaller extends Caller>(name: import("./Hook").HookPluginName, caller: ListenerCaller): import("./Hook").HookListener<Caller, "default", ListenerCaller> | null;
        <ListenerCaller_1 extends Caller>(name: import("./Hook").HookPluginName, mode: "default" | null, caller: ListenerCaller_1): import("./Hook").HookListener<Caller, "default", ListenerCaller_1> | null;
        <ListenerCaller_2 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, mode: "iterator", caller: ListenerCaller_2): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_2> | null;
        <ListenerCaller_3 extends import("./Hook").CallerWithIterator<Caller>>(name: import("./Hook").HookPluginName, iterate: true, caller: ListenerCaller_3): import("./Hook").HookListener<Caller, "iterator", ListenerCaller_3> | null;
        <ListenerCaller_4 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, mode: "context", caller: ListenerCaller_4): import("./Hook").HookListener<Caller, "context", ListenerCaller_4> | null;
        <ListenerCaller_5 extends import("./Hook").CallerWithContext<Caller>>(name: import("./Hook").HookPluginName, iterate: false, caller: ListenerCaller_5): import("./Hook").HookListener<Caller, "context", ListenerCaller_5> | null;
        <Mode extends import("./Hook").CallerMode, ListenerCaller_6 extends import("./Hook").CallerWithMode<Mode, Caller>>(name: import("./Hook").HookPluginName, mode: Mode, caller: ListenerCaller_6): import("./Hook").HookListener<Caller, Mode, ListenerCaller_6> | null;
        <P extends import("./Hook").HookListener<Caller, import("./Hook").CallerMode, Caller | import("./Hook").CallerWithContext<Caller> | import("./Hook").CallerWithIterator<Caller>>>(plugin: P): P | null;
    };
    createIteration(): FilterSyncIterator<T, Caller>;
    filter(...args: FunctionArguments<Caller>): T;
    filterPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>): T;
}
