"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeArray = makeArray;
exports.sortNumbers = sortNumbers;

var _isArray = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/array/is-array"));

function makeArray(input) {
  return (0, _isArray.default)(input) ? input : [input];
}

function sortNumbers(a, b) {
  return Number(a) - Number(b);
}
//# sourceMappingURL=utils.js.map