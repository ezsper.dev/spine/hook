"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.symbol");

require("core-js/modules/es.symbol.description");

require("core-js/modules/es.array.concat");

require("core-js/modules/es.array.iterator");

require("core-js/modules/es.object.to-string");

require("core-js/modules/es.regexp.to-string");

require("core-js/modules/es.string.match");

require("core-js/modules/es.string.split");

require("core-js/modules/web.dom-collections.iterator");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.forgeArgumentNames = forgeArgumentNames;
exports.serializePluginName = serializePluginName;
exports.HookListener = exports.Hook = exports.Interceptor = void 0;

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _sort = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/sort"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _bind = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/bind"));

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _isArray = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/array/is-array"));

var _findIndex = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/find-index"));

var _trim = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/trim"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _splice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/splice"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var _keys = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/keys"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var symbols = _interopRequireWildcard(require("./symbols"));

var _utils = require("./utils");

// maintain, direct symbol importation slow down iteration due to exports.$state$
var $state$ = symbols.$state$;

var Interceptor = function Interceptor() {
  var _this = this;

  (0, _classCallCheck2.default)(this, Interceptor);
  (0, _defineProperty2.default)(this, "interceptors", void 0);
  (0, _defineProperty2.default)(this, "trigger", {});
  (0, _defineProperty2.default)(this, "intercept", function (interceptor) {
    if (_this.interceptors == null) {
      _this.interceptors = [];
    }

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      var _loop = function _loop() {
        var name = _step.value;
        var trigger = _this.trigger[name];

        if (trigger == null) {
          trigger = function trigger() {
            if (_this.interceptors != null) {
              var _iteratorNormalCompletion2 = true;
              var _didIteratorError2 = false;
              var _iteratorError2 = undefined;

              try {
                for (var _iterator2 = _this.interceptors[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                  var _interceptor = _step2.value;
                  var listener = _interceptor[name];

                  if (listener != null) {
                    listener.apply(void 0, arguments);
                  }
                }
              } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                    _iterator2.return();
                  }
                } finally {
                  if (_didIteratorError2) {
                    throw _iteratorError2;
                  }
                }
              }
            }
          };

          _this.trigger[name] = trigger;
        }
      };

      for (var _iterator = (0, _keys.default)(interceptor)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        _loop();
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    _this.interceptors.push(interceptor);

    return function () {
      if (_this.interceptors != null) {
        var _context;

        var index = (0, _indexOf.default)(_context = _this.interceptors).call(_context, interceptor);

        if (index >= 0) {
          var _context2;

          (0, _splice.default)(_context2 = _this.interceptors).call(_context2, index, 1);
          return true;
        }
      }

      return false;
    };
  });
};

exports.Interceptor = Interceptor;

function forgeArgumentNames(caller) {
  var match = caller.toString().match(/\(([^)]+)\)/);

  if (match) {
    var _context3;

    return (0, _map.default)(_context3 = match[1].split(',')).call(_context3, function (value) {
      return (0, _trim.default)(value).call(value);
    });
  }

  return;
}

function iteratorBail(error) {
  var state = this[$state$];

  if (error) {
    state.errorThrown = error;
  }

  state.bailed = true;
  var triggerBail = this.interceptor.trigger.bail;

  if (triggerBail) {
    triggerBail(error);
  }

  return this;
}

function iteratorGoto(name) {
  var _this2 = this;

  // tslint:disable-next-line no-this-assignment
  var self = this;
  var index = (0, _findIndex.default)(self).call(self, name);

  if (index < 0) {
    return false;
  }

  var state = self[$state$];
  state.bailed = true;

  state.return = function () {
    _this2[$state$] = {
      plugins: state.plugins,
      startIndex: index,
      destroyed: false,
      triggerGoto: true
    };
    return self.call.apply(self, arguments);
  };

  var triggerGoto = this.interceptor.trigger.goto;

  if (triggerGoto) {
    triggerGoto(name);
  }

  return true;
}

function iteratorGetState() {
  var state = this[$state$];
  return state.plugins;
}

function iteratorFindIndex(name) {
  var _context4;

  var state = this[$state$];
  var id = serializePluginName(name);
  return (0, _findIndex.default)(_context4 = state.plugins).call(_context4, function (plugin) {
    return serializePluginName(plugin) === id;
  });
}

function iteratorHas(name) {
  var _context5;

  return (0, _findIndex.default)(_context5 = this).call(_context5, name) >= 0;
}

function iteratorEnable(name) {
  var _context6;

  var enable = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var index = (0, _findIndex.default)(_context6 = this).call(_context6, name);
  var state = this[$state$];

  if (index < 0) {
    return false;
  }

  state["disabled".concat(index)] = !enable;
  var triggerEnable = this.interceptor.trigger.enable;

  if (triggerEnable) {
    triggerEnable(name, enable);
  }

  return true;
}

function iteratorDisable(name) {
  return this.enable(name, false);
}

function iteratorStatus(name) {
  var _context7;

  var state = this[$state$];
  var index = (0, _findIndex.default)(_context7 = this).call(_context7, name);

  if (index < 0) {
    return null;
  }

  var key = "disabled".concat(index);

  if (index >= 0 && key in state) {
    return state[key];
  }

  return true;
}

var argVarRegExp = /([a-zA-Z$][a-zA-Z0-9_$]+)/;

function serializePluginName(name) {
  if (name instanceof HookListener) {
    return serializePluginName([name.context, name.name]);
  }

  if ((0, _isArray.default)(name)) {
    return "".concat(name[0] ? "".concat((0, _stringify.default)(typeof name[0] === 'string' ? name[0] : name[0].hookContext), ".") : '').concat((0, _stringify.default)(name[1] || 'default'));
  }

  if (typeof name !== 'string') {
    return "".concat((0, _stringify.default)(name.hookContext), ".").concat((0, _stringify.default)('default'));
  }

  return (0, _stringify.default)(name);
}

var Hook = /*#__PURE__*/function () {
  function Hook(argVars) {
    var defaultPriority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 30;
    (0, _classCallCheck2.default)(this, Hook);
    this.argVars = argVars;
    this.defaultPriority = defaultPriority;
    (0, _defineProperty2.default)(this, $state$, void 0);
    (0, _defineProperty2.default)(this, "interceptor", new Interceptor());

    if (argVars) {
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = argVars[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var argVar = _step3.value;

          if (!argVarRegExp.test(argVar)) {
            throw new Error("Invalid argument variable name \"".concat(argVar, "\""));
          }
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }
    }

    this[$state$] = {
      size: 0,
      pluginMapping: {},
      usingOrdering: 0
    };
  }

  (0, _createClass2.default)(Hook, [{
    key: "intercept",
    value: function intercept(interceptor) {
      return this.interceptor.intercept(interceptor);
    }
  }, {
    key: "has",
    value: function has(pluginOrName) {
      var plugin = this[$state$].pluginMapping[serializePluginName(name)];

      if (plugin == null) {
        return false;
      }

      if (pluginOrName instanceof HookListener) {
        return pluginOrName === plugin;
      }

      return true;
    }
  }, {
    key: "get",
    value: function get(name) {
      var plugin = this[$state$].pluginMapping[serializePluginName(name)];

      if (plugin) {
        return plugin;
      }

      return null;
    }
  }, {
    key: "bind",
    value: function bind(name) {
      for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        rest[_key - 1] = arguments[_key];
      }

      var callerIndex = typeof rest[0] !== 'function' ? 1 : 0;
      var state = this[$state$];
      var plugin = typeof name === 'object' && !(0, _isArray.default)(name) && name instanceof HookListener ? name : new HookListener(name, this, rest[0] === true ? 'iterator' : rest[0] === false ? 'context' : typeof rest[0] === 'string' ? rest[0] : 'default', rest[callerIndex], typeof rest[callerIndex + 1] === 'number' || rest[callerIndex + 1] == null ? undefined : (0, _utils.makeArray)(rest[callerIndex + 1]), typeof rest[callerIndex + 1] === 'number' ? rest[callerIndex + 1] : rest[callerIndex + 2]);

      if (plugin.hook !== this) {
        throw new Error("Owned by another hook");
      }

      var id = serializePluginName(name);

      if (state.pluginMapping[id] === plugin) {
        return plugin;
      }

      state.pluginMapping[id] = plugin;

      if (plugin.priority || plugin.order && plugin.order.length) {
        this[$state$].usingOrdering += 1;
      }

      delete state.pluginOrdering;
      delete state.pluginIteration;
      state.size += 1;
      var bindTrigger = (0, _bind.default)(this.interceptor.trigger);

      if (bindTrigger) {
        bindTrigger(plugin);
      }

      var pluginBindTrigger = (0, _bind.default)(plugin.interceptor.trigger);

      if (pluginBindTrigger) {
        pluginBindTrigger(this);
      }

      return plugin;
    }
  }, {
    key: "unbind",
    value: function unbind(name) {
      var state = this[$state$];
      var id = serializePluginName(name);
      var plugin = state.pluginMapping[id];

      if (name instanceof HookListener && plugin !== name) {
        return null;
      }

      if (plugin != null) {
        var _ref;

        if ((arguments.length <= 1 ? 0 : arguments.length - 1) === 0 || (typeof (arguments.length <= 1 ? undefined : arguments[1]) === 'function' || ((arguments.length <= 1 ? undefined : arguments[1]) === true ? 'iterator' : (arguments.length <= 1 ? undefined : arguments[1]) === false ? 'context' : arguments.length <= 1 ? undefined : arguments[1]) === plugin.mode) && (typeof (arguments.length <= 1 ? undefined : arguments[1]) === 'function' || (arguments.length <= 1 ? 0 : arguments.length - 1) > 1 ? plugin.caller === (_ref = ((arguments.length <= 1 ? 0 : arguments.length - 1) > 1 ? 1 : 0) + 1, _ref < 1 || arguments.length <= _ref ? undefined : arguments[_ref]) : true)) {
          delete state.pluginMapping[id];

          if (plugin.priority || plugin.order && plugin.order.length) {
            state.usingOrdering += -1;
          }

          delete state.pluginOrdering;
          delete state.pluginIteration;
          state.size += -1;
          var unbindTrigger = this.interceptor.trigger.unbind;

          if (unbindTrigger) {
            unbindTrigger(plugin);
          }

          var pluginUnbindTrigger = plugin.interceptor.trigger.unbind;

          if (pluginUnbindTrigger) {
            pluginUnbindTrigger(this);
          }

          return plugin;
        }
      }

      return null;
    }
  }, {
    key: "plugins",
    value: function plugins() {
      var state = this[$state$];
      var plugins = state.pluginOrdering;

      if (plugins != null) {
        return (0, _slice.default)(plugins).call(plugins, 0);
      }

      plugins = [];
      var mappingPlugins = this[$state$].pluginMapping;

      if (mappingPlugins != null) {
        if (this[$state$].usingOrdering < 1) {
          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            for (var _iterator4 = (0, _keys.default)(mappingPlugins)[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              var _pluginName = _step4.value;
              plugins.push(mappingPlugins[_pluginName]);
            }
          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                _iterator4.return();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }
        } else {
          var _order = {};
          var before;
          var after;
          var _iteratorNormalCompletion5 = true;
          var _didIteratorError5 = false;
          var _iteratorError5 = undefined;

          try {
            for (var _iterator5 = (0, _keys.default)(mappingPlugins)[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
              var _pluginId = _step5.value;
              var _plugin = mappingPlugins[_pluginId];
              var matchOrder = false;

              if (_plugin.order != null) {
                var _iteratorNormalCompletion7 = true;
                var _didIteratorError7 = false;
                var _iteratorError7 = undefined;

                try {
                  for (var _iterator7 = _plugin.order[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                    var match = _step7.value;

                    if ('before' in match) {
                      var id = serializePluginName(match.before);

                      if (mappingPlugins[id] != null) {
                        if (before == null) {
                          before = {};
                        }

                        if (before[id] == null) {
                          before[id] = [];
                        }

                        before[id].push(_plugin);
                        matchOrder = true;
                        break;
                      }
                    } else if ('after' in match) {
                      var _id = serializePluginName(match.after);

                      if (mappingPlugins[_id] != null) {
                        if (after == null) {
                          after = {};
                        }

                        if (after[_id] == null) {
                          after[_id] = [];
                        }

                        after[_id].push(_plugin);

                        matchOrder = true;
                        break;
                      }
                    }
                  }
                } catch (err) {
                  _didIteratorError7 = true;
                  _iteratorError7 = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion7 && _iterator7.return != null) {
                      _iterator7.return();
                    }
                  } finally {
                    if (_didIteratorError7) {
                      throw _iteratorError7;
                    }
                  }
                }
              }

              var _priority = _plugin.priority || this.defaultPriority;

              if (!matchOrder) {
                if (_order[_priority] == null) {
                  _order[_priority] = [];
                }

                _order[_priority].push(_plugin);
              }
            }
          } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                _iterator5.return();
              }
            } finally {
              if (_didIteratorError5) {
                throw _iteratorError5;
              }
            }
          }

          var _iteratorNormalCompletion6 = true;
          var _didIteratorError6 = false;
          var _iteratorError6 = undefined;

          try {
            for (var _iterator6 = (0, _sort.default)(_context8 = (0, _keys.default)(_order)).call(_context8, _utils.sortNumbers)[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
              var _context8;

              var _priority2 = _step6.value;
              var _iteratorNormalCompletion8 = true;
              var _didIteratorError8 = false;
              var _iteratorError8 = undefined;

              try {
                for (var _iterator8 = _order[_priority2][Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                  var _plugin2 = _step8.value;

                  var _id2 = serializePluginName(_plugin2);

                  var beforePlugins = before != null && before[_id2] != null ? before[_id2] : undefined;
                  var afterPlugins = after != null && after[_id2] != null ? after[_id2] : undefined;

                  if (beforePlugins != null) {
                    var _iteratorNormalCompletion9 = true;
                    var _didIteratorError9 = false;
                    var _iteratorError9 = undefined;

                    try {
                      for (var _iterator9 = beforePlugins[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                        var _plugin3 = _step9.value;
                        plugins.push(_plugin3);
                      }
                    } catch (err) {
                      _didIteratorError9 = true;
                      _iteratorError9 = err;
                    } finally {
                      try {
                        if (!_iteratorNormalCompletion9 && _iterator9.return != null) {
                          _iterator9.return();
                        }
                      } finally {
                        if (_didIteratorError9) {
                          throw _iteratorError9;
                        }
                      }
                    }
                  }

                  plugins.push(_plugin2);

                  if (afterPlugins != null) {
                    var _iteratorNormalCompletion10 = true;
                    var _didIteratorError10 = false;
                    var _iteratorError10 = undefined;

                    try {
                      for (var _iterator10 = afterPlugins[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
                        var _plugin4 = _step10.value;
                        plugins.push(_plugin4);
                      }
                    } catch (err) {
                      _didIteratorError10 = true;
                      _iteratorError10 = err;
                    } finally {
                      try {
                        if (!_iteratorNormalCompletion10 && _iterator10.return != null) {
                          _iterator10.return();
                        }
                      } finally {
                        if (_didIteratorError10) {
                          throw _iteratorError10;
                        }
                      }
                    }
                  }
                }
              } catch (err) {
                _didIteratorError8 = true;
                _iteratorError8 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion8 && _iterator8.return != null) {
                    _iterator8.return();
                  }
                } finally {
                  if (_didIteratorError8) {
                    throw _iteratorError8;
                  }
                }
              }
            }
          } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
                _iterator6.return();
              }
            } finally {
              if (_didIteratorError6) {
                throw _iteratorError6;
              }
            }
          }
        }
      }

      state.pluginOrdering = plugins;
      return plugins;
    }
  }, {
    key: "iterate",
    value: function iterate(context) {
      var state = this[$state$];
      var call = state.pluginIteration;

      if (!call) {
        state.pluginIteration = call = this.createIteration();
      }

      var iteratorContext;
      var passingContext = false;

      if (context) {
        iteratorContext = context;
        passingContext = true;
      } else {
        iteratorContext = {};
      }

      var interceptor = new Interceptor();
      var iterator = {
        call,
        passingContext,
        interceptor,
        intercept: interceptor.intercept,
        context: iteratorContext,
        bail: iteratorBail,
        goto: iteratorGoto,
        enable: iteratorEnable,
        disable: iteratorDisable,
        status: iteratorStatus,
        findIndex: iteratorFindIndex,
        getState: iteratorGetState,
        has: iteratorHas,
        hook: this,
        [$state$]: {
          plugins: state.pluginOrdering,
          startIndex: 0,
          destroyed: false
        }
      };
      var triggerIterate = this.interceptor.trigger.iterate;

      if (triggerIterate) {
        triggerIterate(iterator);
      }

      return iterator;
    }
  }, {
    key: "size",
    get: function get() {
      return this[$state$].size;
    }
  }]);
  return Hook;
}();

exports.Hook = Hook;

var HookListener = /*#__PURE__*/function () {
  function HookListener(name, hook, mode, caller, order, priority) {
    (0, _classCallCheck2.default)(this, HookListener);
    this.hook = hook;
    this.mode = mode;
    this.caller = caller;
    this.order = order;
    this.priority = priority;
    (0, _defineProperty2.default)(this, "disabled", void 0);
    (0, _defineProperty2.default)(this, "name", void 0);
    (0, _defineProperty2.default)(this, "contextValue", void 0);
    (0, _defineProperty2.default)(this, "context", void 0);
    (0, _defineProperty2.default)(this, "interceptor", new Interceptor());
    (0, _defineProperty2.default)(this, "callerLength", void 0);

    if ((0, _isArray.default)(name)) {
      var contextValue = name[0];
      this.contextValue = contextValue;
      this.context = contextValue == null ? contextValue : typeof contextValue === 'string' ? contextValue : contextValue.hookContext;
      this.name = name[1] || 'default';
    } else if (typeof name !== 'string') {
      this.contextValue = name;
      this.context = name.hookContext;
      this.name = 'default';
    } else {
      this.name = name;
    }
  }

  (0, _createClass2.default)(HookListener, [{
    key: "intercept",
    value: function intercept(interceptor) {
      return this.interceptor.intercept(interceptor);
    }
  }, {
    key: "bind",
    value: function bind() {
      var _context9;

      return (0, _bind.default)(_context9 = this.hook).call(_context9, this);
    }
  }, {
    key: "unbind",
    value: function unbind() {
      return this.hook.unbind(this);
    }
    /**
     * Usable when resting arguments
     * @param length
     */

  }, {
    key: "args",
    value: function args(length) {
      this.callerLength = length;
      return this;
    }
  }, {
    key: "enable",
    value: function enable() {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.disabled = !state;
      var enableTrigger = this.interceptor.trigger.enable;

      if (enableTrigger) {
        enableTrigger(state);
      }

      return this;
    }
  }, {
    key: "disable",
    value: function disable() {
      return this.enable(false);
    }
  }, {
    key: "toString",
    value: function toString() {
      return "[HookListener ".concat(serializePluginName(this), "]");
    }
  }, {
    key: "binded",
    get: function get() {
      return this.hook.has(this);
    }
  }]);
  return HookListener;
}();

exports.HookListener = HookListener;
//# sourceMappingURL=Hook.js.map