import { Hook } from './Hook';
export declare function trace(hook: Hook, namespace?: string, stack?: boolean, manyStack?: number): () => boolean;
