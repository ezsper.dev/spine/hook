"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  HookFilterSync: true
};
exports.HookFilterSync = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _bind = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/bind"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _Hook2 = require("./Hook");

Object.keys(_Hook2).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Hook2[key];
    }
  });
});

var _filterSync = require("./factory/filterSync");

var HookFilterSync = /*#__PURE__*/function (_Hook) {
  (0, _inherits2.default)(HookFilterSync, _Hook);

  function HookFilterSync() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, HookFilterSync);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(HookFilterSync)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "addFilter", (0, _bind.default)((0, _assertThisInitialized2.default)(_this)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "removeFilter", _this.unbind);
    return _this;
  }

  (0, _createClass2.default)(HookFilterSync, [{
    key: "createIteration",
    value: function createIteration() {
      return (0, _filterSync.createFilterSyncIterator)(this);
    }
  }, {
    key: "filter",
    value: function filter() {
      var _this$iterate;

      return (_this$iterate = this.iterate()).call.apply(_this$iterate, arguments);
    }
  }, {
    key: "filterPassing",
    value: function filterPassing(context) {
      var _this$iterate2;

      for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        args[_key2 - 1] = arguments[_key2];
      }

      return (_this$iterate2 = this.iterate(context)).call.apply(_this$iterate2, args);
    }
  }], [{
    key: "template",
    value: function template(caller) {
      return new HookFilterSync((0, _Hook2.forgeArgumentNames)(caller));
    }
  }]);
  return HookFilterSync;
}(_Hook2.Hook);

exports.HookFilterSync = HookFilterSync;
//# sourceMappingURL=HookFilterSync.js.map