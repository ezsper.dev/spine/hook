"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  HookActionSync: true
};
exports.HookActionSync = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _bind = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/bind"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _Hook2 = require("./Hook");

Object.keys(_Hook2).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Hook2[key];
    }
  });
});

var _actionSync = require("./factory/actionSync");

var HookActionSync = /*#__PURE__*/function (_Hook) {
  (0, _inherits2.default)(HookActionSync, _Hook);

  function HookActionSync() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, HookActionSync);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(HookActionSync)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "addAction", (0, _bind.default)((0, _assertThisInitialized2.default)(_this)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "removeAction", _this.unbind);
    return _this;
  }

  (0, _createClass2.default)(HookActionSync, [{
    key: "createIteration",
    value: function createIteration() {
      return (0, _actionSync.createActionSyncIterator)(this);
    }
  }, {
    key: "do",
    value: function _do() {
      var _this$iterate;

      return (_this$iterate = this.iterate()).call.apply(_this$iterate, arguments);
    }
  }, {
    key: "doPassing",
    value: function doPassing(context) {
      var _this$iterate2;

      for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        args[_key2 - 1] = arguments[_key2];
      }

      return (_this$iterate2 = this.iterate(context)).call.apply(_this$iterate2, args);
    }
  }], [{
    key: "template",
    value: function template(caller) {
      return new HookActionSync((0, _Hook2.forgeArgumentNames)(caller));
    }
  }]);
  return HookActionSync;
}(_Hook2.Hook);

exports.HookActionSync = HookActionSync;
//# sourceMappingURL=HookActionSync.js.map