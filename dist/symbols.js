"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.$state$ = void 0;

var _for = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/symbol/for"));

/**
 * Private state symbol not exported on index
 */
var $state$ = (0, _for.default)('state');
exports.$state$ = $state$;
//# sourceMappingURL=symbols.js.map