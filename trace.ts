import { Hook, serializePluginName } from './Hook';

function getStack() {
  // Save original Error.prepareStackTrace
  const origPrepareStackTrace = Error.prepareStackTrace;

  // Override with function that just returns `stack`
  Error.prepareStackTrace = function(_, stack) {
    return stack;
  };

  // Create a new `Error`, which automatically gets `stack`
  const err = new Error();

  // Evaluate `err.stack`, which calls our new `Error.prepareStackTrace`
  const stack: NodeJS.CallSite[] = <any>err.stack;

  // Restore original `Error.prepareStackTrace`
  Error.prepareStackTrace = origPrepareStackTrace;

  stack.shift(); // getStack

  return stack;
}

const supportStack = true;

function formatStack(start = 0, many = 1) {
  const stack = getStack().slice(4 + start, 4 + start + many);
  return `\n  ${stack.map(callSite => {
    const functionName = callSite.getFunctionName();
    const fileNameWithLine = `${callSite.getFileName()}:${callSite.getLineNumber()}:${callSite.getColumnNumber()}`;
    return `at ${functionName ? `${functionName} (${fileNameWithLine})` : `${fileNameWithLine}`}`;
  }).join('\n  ')}`
}

function inspectValue(value: any) {
  if ('process' in global) {
    const util = require('util');
    return util.inspect(value);
  }
  return JSON.stringify(value);
}

function printInspect(message: string, stack = true, start = 0, many = 1) {
  if (stack && supportStack) {
    // tslint:disable-next-line no-parameter-reassignment
    message += formatStack(1 + start, many);
  }
  console.log(message);
}

export function trace(hook: Hook, namespace?: string, stack = true, manyStack = 1) {
  let iteratorId = -1;
  const printNamespace = namespace ? `${namespace}: ` : '';
  const unsubs: (() => void)[] = [];
  let unsubscribed = false;
  unsubs.push(
    hook.intercept({
      bind(plugin) {
        const priority = `${plugin.priority == null ? `default ${hook.defaultPriority}` : plugin.priority}`;
        const message = `${printNamespace}Plugin ${serializePluginName(plugin)} was binded with (mode: ${
          plugin.mode
        }, ${
          plugin.order && plugin.order.length
            ? `order: ${inspectValue(plugin.order)}, priorityFallback: ${priority}`
            : `priority: ${priority}`
        })`;
        printInspect(message, stack, 0, manyStack);
        unsubs.push(
          plugin.intercept({
            enable(state) {
              printInspect(`${printNamespace}Plugin ${serializePluginName(plugin)} was ${state ? 'enabled' : 'disabled'}`, stack, 0, manyStack);
            },
          }),
        );
      },
      unbind(plugin) {
        printInspect(`${printNamespace}Plugin ${serializePluginName(plugin)} was unbind`, stack, 0, manyStack);
      },
      iterate(iterator) {
        iteratorId += 1;
        const id = iteratorId;
        printInspect(`${printNamespace}Iterator#${id} created`, stack, 1, manyStack);
        unsubs.push(
          iterator.intercept({
            call(...args) {
              const argPrint: string[] = [];
              for (let i = 0; i < args.length; i += 1) {
                argPrint.push(`${hook.argVars && hook.argVars[i] ? hook.argVars[i] : `arg_${i}`}: ${inspectValue(args[i])}`);
              }
              printInspect(`${printNamespace}Iterator#${id} called with (${argPrint.join(', ')})`, stack, 1, manyStack);
            },
            bail(error) {
              printInspect(`${printNamespace}Iterator#${id} bailed with${error ? '' : 'out'} error`, stack, 0, manyStack);
            },
            enable(name, state) {
              printInspect(`${printNamespace}Iterator#${id} ${state ? 'enabled' : 'disabled'} "${name}"`, stack, 1, manyStack);
            },
            goto(name) {
              printInspect(`${printNamespace}Iterator#${id} skipped to "${name}"`, stack, 0, manyStack);
            },
          }),
        );
      },
    }),
  );
  return () => {
    if (!unsubscribed) {
      unsubscribed = true;
      for (const unsubscribe of unsubs) {
        unsubscribe();
      }
      return true;
    }
    return false;
  };
}
