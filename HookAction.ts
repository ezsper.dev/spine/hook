import {
  Hook,
  Promiseable,
  CallerFn,
  HookIteratorContext,
  FunctionArguments,
  forgeArgumentNames,
} from './Hook';
import {
  createActionIterator,
  ActionIterator,
} from './factory/action';

export * from './Hook';

export class HookAction<Caller extends CallerFn = CallerFn> extends Hook<Caller, Promise<void>> {

  static template<Caller extends CallerFn = CallerFn>(caller: Caller): HookAction<(...args: FunctionArguments<Caller>) => Promiseable<ReturnType<Caller>>> {
    return new HookAction(forgeArgumentNames(caller));
  }

  addAction = this.bind;
  removeAction = this.unbind;

  protected createIteration(): ActionIterator<Caller> {
    return createActionIterator<Caller>(this);
  }

  do(...args: FunctionArguments<Caller>) {
    return this.iterate().call(...args);
  }

  doPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>) {
    return this.iterate(context).call(...args);
  }

}
