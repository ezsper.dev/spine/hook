import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import {
  mapArguments,
  iteratorOuterHead,
  iteratorHead,
  iteratorDestroy,
  iteratorErrorThrown,
} from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type ActionIterator<Caller extends (...args: any[]) => any> = (this: HookIterator<Caller, Promise<void>>, ...args: FunctionArguments<Caller>) => Promise<void>;

export function createActionIterator<Caller extends (...args: any[]) => any>(hook: Hook<Caller, any>): ActionIterator<Caller> {
  const plugins = hook.plugins();
  const iterate = () => `${iteratorHead(hook)}
var _promises = [];
var _promise;
${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
${plugins.map((plugin, i) => plugin.disabled ? `_state.disabled${i} = true;\n` : '').join('')}
${plugins.map((plugin, i) => `
  if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
    _promise = _caller${i}(${plugin.mode !== 'default' ? (plugin.mode === 'iterator' ? '_iterator' : '_context') : ''}${plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0 ))});
    _promises.push(_promise);
  }
`).join('\n  ')}
return Promise.all(_promises)
  .then(function() {
    ${iteratorErrorThrown}
    ${iteratorDestroy}
    if (_state.return) {
      return _state.return.apply(this, arguments);
    }
  })
  .catch(function (error) {
    ${iteratorDestroy}
    throw error;
  });`;
  return Function(`${iteratorOuterHead}
${plugins.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${plugins.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
return function _iterate() {
${iterate()}
}`).call({ $state$, plugins });
}