import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import {
  mapArguments,
  iteratorOuterHead,
  iteratorHead,
  iteratorDestroy,
  iteratorErrorThrown,
} from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type ActionSyncIterator<Caller extends (...args: any[]) => any> = (this: HookIterator<Caller, void>, ...args: FunctionArguments<Caller>) => void;

export function createActionSyncIterator<Caller extends (...args: any[]) => any>(hook: Hook<Caller, any>): ActionSyncIterator<Caller> {
  const plugins = hook.plugins();
  const iterate = () => `try {
${iteratorHead(hook)}
${plugins.map((plugin, i) => plugin.disabled ? `_state.disabled${i} = true;\n` : '').join('')}
${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
${plugins.map((plugin, i) => `if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
  _caller${i}(${plugin.mode !== 'default' ? (plugin.mode === 'iterator' ? '_iterator' : '_context') : ''}${plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0 ))});
}`).join('\n  ')}
${iteratorErrorThrown}
${iteratorDestroy}
if (_state.return) {
  return _state.return.apply(this, arguments);
}
} catch (error) {
${iteratorDestroy}
throw error;
}`;
  return Function(`${iteratorOuterHead}
${plugins.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${plugins.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
return function _iterate() {
${iterate()}
}`).call({ $state$, plugins });
}