import { Hook } from '../Hook';

export const iteratorOuterHead = `"use strict";
var _self = this;
var $state$ = _self.$state$;`;
export const iteratorHead = (hook: Hook) => `var _iterator = this;
var _hook = _iterator.hook;
var _interceptorHook = _hook.interceptor;
var _state = _iterator[$state$];
var _context = _iterator.context;
var _interceptor = _iterator.interceptor;
var _triggerCall = _interceptor.trigger.call;
if (_triggerCall && !_state.triggerGoto) {
  _triggerCall.apply(null, arguments);
}
var _triggerHookCall = _interceptorHook.trigger.call;
if (_triggerHookCall && !_state.triggerGoto) {
  _triggerHookCall.apply(null, [_iterator].concat(Array.from(arguments)));
}
`;
export const iteratorDestroy = `
_state.destroyed = true;
`;
export const iteratorErrorThrown = `if (_state.errorThrown) {
  throw _state.errorThrown;
}`;

export function mapArguments(names: string[] | undefined, length: number, mapArgument: (index: number, name: string | undefined) => string | undefined | null = (index) => undefined) {
  return mapArgumentsPrefix('', names, length, mapArgument);
}

export function mapArgumentsPrefix(prefix: string, names: string[] | undefined, length: number, mapArgument: (index: number, name: string | undefined) => string | undefined | null = (index) => undefined) {
  return Array(length)
    .join('.')
    .split('.')
    .map((empty, index) => mapArgument(index, names ? names[index] : undefined) || (names && names[index] ? names[index] : `${prefix}arguments[${index}]`))
    .join(', ');
}