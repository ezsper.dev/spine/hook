import { Hook, Promiseable, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import {
  mapArgumentsPrefix,
  iteratorOuterHead,
  iteratorHead,
  iteratorDestroy,
  iteratorErrorThrown,
} from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type FilterCaller<T> = (...args: [T, ...any[]]) => Promiseable<T>;
export type FilterIterator<T, Caller extends FilterCaller<T>> = (this: HookIterator<Caller, Promise<T>>, ...args: FunctionArguments<Caller>) => Promise<T>;

export function createFilterIterator<T, Caller extends FilterCaller<any>>(hook: Hook<Caller, Promise<T>>): FilterIterator<T, Caller> {
  const plugins = hook.plugins();
  const iterate = () => `${iteratorHead(hook)}
${plugins.map((plugin, i) => plugin.disabled ? `_state.disabled${i} = true;\n` : '').join('')}
var _arguments = arguments;
${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
${hook.argVars ? '' : 'var _value = arguments[0];'}
var _promise = Promise.resolve(${hook.argVars ? hook.argVars[0] : '_value'});
${plugins.map((plugin, i) => `_promise = _promise.then(function(${hook.argVars ? hook.argVars[0] : '_value'}) {
  if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
    return _caller${i}(${plugin.mode !== 'default' ? (plugin.mode === 'iterator' ? '_iterator' : '_context') : ''}${plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArgumentsPrefix('_', hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0 ), (index) => index === 0 && !hook.argVars ? '_value' : undefined)});
  }
  return ${hook.argVars ? hook.argVars[0] : '_value'};
})`).join('\n  ')}
return _promise
  .then(function(${hook.argVars ? hook.argVars[0] : '_value'}) {
    ${iteratorErrorThrown}
    ${iteratorDestroy}
    if (_state.return) {
      var __arguments = Array.from(_arguments);
      __arguments[0] = ${hook.argVars ? hook.argVars[0] : '_value'};
      return _state.return.apply(this, __arguments);
    }
    return ${hook.argVars ? hook.argVars[0] : '_value'};
  })
  .catch(function (error) {
    ${iteratorDestroy}
    throw error;
  });`;
  return Function(`${iteratorOuterHead}
${plugins.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${plugins.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
return function _iterate() {
${iterate()}
}`).call({ $state$, plugins});
}
/*import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import { mapArguments } from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type FilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T> = (this: HookIterator<Caller>, ...args: FunctionArguments<Caller>) => T;

export function createFilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T>(hook: Hook<Caller>): FilterSyncIterator<T, Caller> {
  const state = hook[$state$];
  return Function(`"use strict";
var _self = this;
var $state$ = _self.$state$;
${state.pluginOrder.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${state.pluginOrder.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
function _iterate() {
  var _iterator = this;
  var _state = this[$state$];
  var _value = arguments[0];
  ${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
  ${state.pluginOrder.map((plugin, i) => `if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
    _value = _caller${i}(${plugin.iterate ? '_iterator' : ''}${plugin.iterate && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.iterate ? -1 : 0 ))});
  }`).join('\n  ')}
  _state.destroyed = true;
  if (_state.errorThrown) {
    throw _state.errorThrown;
  }
  return _value;
}
return _iterate;`).call({ $state$, plugins: state.pluginOrder });
}
*/