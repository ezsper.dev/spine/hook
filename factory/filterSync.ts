import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import {
  mapArguments,
  iteratorOuterHead,
  iteratorHead,
  iteratorDestroy,
  iteratorErrorThrown,
} from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type FilterSyncCaller<T> = (...args: [T, ...any[]]) => T;
export type FilterSyncIterator<T, Caller extends FilterSyncCaller<T>> = (this: HookIterator<Caller, T>, ...args: FunctionArguments<Caller>) => T;

export function createFilterSyncIterator<T, Caller extends FilterSyncCaller<T>>(hook: Hook<Caller, T>): FilterSyncIterator<T, Caller> {
  const plugins = hook.plugins();
  const iterate = () => `try {
${iteratorHead(hook)}
${plugins.map((plugin, i) => plugin.disabled ? `_state.disabled${i} = true;\n` : '').join('')}
${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
${hook.argVars ? '' : 'var _value = arguments[0];'}
${plugins.map((plugin, i) => `if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
  ${hook.argVars ? hook.argVars[0] : '_value'} = _caller${i}(${plugin.mode !== 'default' ? (plugin.mode === 'iterator' ? '_iterator' : '_context') : ''}${plugin.mode !== 'default' && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.mode !== 'default' ? -1 : 0 ), (index) => index === 0 && !hook.argVars ? '_value' : undefined)});
}`).join('\n  ')}
${iteratorErrorThrown}
${iteratorDestroy}
if (_state.return) {
  var _arguments = Array.from(arguments);
  _arguments[0] = ${hook.argVars ? hook.argVars[0] : '_value'};
  return _state.return.apply(this, _arguments);
}
return ${hook.argVars ? hook.argVars[0] : '_value'};
} catch (error) {
${iteratorDestroy}
  throw error;
}`;
  return Function(`${iteratorOuterHead}
${plugins.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${plugins.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
return function _iterate() {
${iterate()}
}`).call({ $state$, plugins });
}
/*import { Hook, FunctionArguments, HookIterator } from '../Hook';
import * as symbols from '../symbols';
import { mapArguments } from './shared';

// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type FilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T> = (this: HookIterator<Caller>, ...args: FunctionArguments<Caller>) => T;

export function createFilterSyncIterator<T, Caller extends (...args: [T, ...any[]]) => T>(hook: Hook<Caller>): FilterSyncIterator<T, Caller> {
  const state = hook[$state$];
  return Function(`"use strict";
var _self = this;
var $state$ = _self.$state$;
${state.pluginOrder.map((plugin, i) => `var _plugin${i} = _self.plugins[${i}];`).join('\n')}
${state.pluginOrder.map((plugin, i) => `var _caller${i} = _plugin${i}.caller;`).join('\n')}
function _iterate() {
  var _iterator = this;
  var _state = this[$state$];
  var _value = arguments[0];
  ${hook.argVars ? (<any>hook).argVars.map((name: string, index: number) => `var ${name} = arguments[${index}];`).join('\n') : ''}
  ${state.pluginOrder.map((plugin, i) => `if (_state.startIndex <= ${i} && ${i > 0 ? `!_state.bailed && ` : ''}!_state.disabled${i}) {
    _value = _caller${i}(${plugin.iterate ? '_iterator' : ''}${plugin.iterate && (plugin.callerLength || plugin.caller.length) > 1 ? ', ' : ''}${mapArguments(hook.argVars,(plugin.callerLength || plugin.caller.length) + (plugin.iterate ? -1 : 0 ))});
  }`).join('\n  ')}
  _state.destroyed = true;
  if (_state.errorThrown) {
    throw _state.errorThrown;
  }
  return _value;
}
return _iterate;`).call({ $state$, plugins: state.pluginOrder });
}
*/