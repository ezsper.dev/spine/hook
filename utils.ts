export function makeArray<T>(input: T): T extends any[] ? T : T[] {
  return <any>(Array.isArray(input) ? input : [input]);
}

export function sortNumbers(a: string | number, b: string | number) {
  return Number(a) - Number(b);
}