/**
 * Private state symbol not exported on index
 */
export const $state$ = Symbol.for('state');