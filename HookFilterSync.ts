import {
  Hook,
  CallerFn,
  FunctionArguments,
  forgeArgumentNames,
  HookIteratorContext,
} from './Hook';
import {
  createFilterSyncIterator,
  FilterSyncIterator,
} from './factory/filterSync';

export * from './Hook';

export type FilterSyncCallerFn<T> = CallerFn<[T, ...any[]], T>;

export class HookFilterSync<T = any, Caller extends FilterSyncCallerFn<T> = FilterSyncCallerFn<T>> extends Hook<Caller, T> {

  static template<T, Caller extends FilterSyncCallerFn<T>>(caller: FilterSyncCallerFn<T> & Caller): HookFilterSync<T, Caller> {
    return new HookFilterSync(forgeArgumentNames(caller));
  }

  addFilter = this.bind;
  removeFilter = this.unbind;

  createIteration(): FilterSyncIterator<T, Caller> {
    return createFilterSyncIterator<T, Caller>(this);
  }

  filter(...args: FunctionArguments<Caller>) {
    return this.iterate().call(...args);
  }

  filterPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>) {
    return this.iterate(context).call(...args);
  }

}