import * as symbols from './symbols';
import { makeArray, sortNumbers } from './utils';
// maintain, direct symbol importation slow down iteration due to exports.$state$
const $state$: typeof symbols.$state$ = symbols.$state$;

export type Promiseable<T> = T extends Promise<infer U> ? (U | T) : (T | Promise<T>);

export type FunctionArguments<Func extends CallerFn> =
  Func extends (...args: infer Args) => any ? Args : never;

export type InterceptorEvents = { [name: string]: Function };

export class Interceptor<Trigger extends InterceptorEvents> {

  private interceptors?: Partial<Trigger>[];

  readonly trigger: Partial<Trigger> = {};

  constructor() {}

  intercept = (interceptor: Partial<Trigger>) => {
    if (this.interceptors == null) {
      this.interceptors = [];
    }
    for (const name of Object.keys(interceptor)) {
      let trigger = this.trigger[name];
      if (trigger == null) {
        trigger = ((...args: any[]) => {
          if (this.interceptors != null) {
            for (const interceptor of this.interceptors) {
              const listener = interceptor[name];
              if (listener != null) {
                listener(...args);
              }
            }
          }
        }) as any;
        (<any>this.trigger)[name] = trigger;
      }
    }
    this.interceptors.push(interceptor);
    return () => {
      if (this.interceptors != null) {
        const index = this.interceptors.indexOf(interceptor);
        if (index >= 0) {
          this.interceptors.splice(index, 1);
          return true;
        }
      }
      return false;
    };
  }

}

export type HookIteratorContext = Record<string, any>;

interface HookIteratorState<Caller extends CallerFn, Result = void> {
  plugins: ReadonlyArray<HookListener<Caller>>;
  startIndex: number;
  bailed?: boolean;
  return?: (...args: FunctionArguments<Caller>) => Result,
  errorThrown?: Error;
  triggerGoto?: boolean;
  destroyed?: boolean;
}

export type HookIteratorEvents<Caller extends (...args: any[]) => any, IterateCallResult = any> = {
  call(...args: FunctionArguments<Caller>): void;
  bail(error?: Error): void;
  goto(name: HookPluginName): void;
  enable(name: HookPluginName, state: boolean): void;
}

export interface HookIterator<Caller extends CallerFn, Result = void> {
  /**
   * Hook owner
   */
  readonly hook: Hook<Caller, Result>;

  /**
   * Passing context
   */
  readonly passingContext: boolean;

  /**
   * Iterator context
   */
  readonly context: HookIteratorContext;
  /**
   * Iterator interceptor
   */
  intercept(interceptor: Partial<HookIteratorEvents<Caller, Result>>): () => boolean;
  /**
   * Iterator interceptor
   */
  readonly interceptor: Interceptor<HookIteratorEvents<Caller, Result>>;
  /**
   * Execute iterator
   * @param args
   */
  call(...args: FunctionArguments<Caller>): Result;

  /**
   * Bail iteration
   * @param error
   */
  bail(error?: Error): this;

  /**
   * Find plugin index by name
   * @param name
   */
  findIndex(name: HookPluginName): number;

  /**
   * Get iterator plugin state
   */
  getState(): ReadonlyArray<HookListener<Caller>>;

  /**
   * Check if plugin exists by name
   * @param name
   */
  has(name: HookPluginName): boolean;

  /**
   * Go to plugin by name
   * @param name
   * @result Return true if plugin is found
   */
  goto(name: HookPluginName): boolean;

  /**
   * Check if plugin is enabled by name
   * @param name
   * @result Return the boolean status or null if plugin is not found
   */
  status(name: HookPluginName): boolean | null;

  /**
   * Enable a plugin by name
   * @param name
   * @param enable Enable status
   */
  enable(name: HookPluginName, enable?: boolean): boolean;

  /**
   * Disable a plugin by name
   * @param name
   */
  disable(name: HookPluginName): boolean;
}

type HookIteratorWithState<Caller extends CallerFn, IteratorCallResult = void> = HookIterator<Caller, IteratorCallResult> & { [$state$]: HookIteratorState<Caller, IteratorCallResult> };

export function forgeArgumentNames<Caller extends CallerFn>(caller: Caller): ArgumentNames<Caller> | undefined {
  const match = caller.toString().match(/\(([^)]+)\)/);
  if (match) {
    return <any>match[1].split(',').map(value => value.trim());
  }
  return;
}

function iteratorBail<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, error?: Error) {
  const state = this[$state$];
  if (error) {
    state.errorThrown = error;
  }
  state.bailed = true;
  const triggerBail = this.interceptor.trigger.bail;
  if (triggerBail) {
    triggerBail(error);
  }
  return this;
}

function iteratorGoto<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: HookPluginName) {
  // tslint:disable-next-line no-this-assignment
  const self = this;
  const index = self.findIndex(name);
  if (index < 0) {
    return false;
  }
  const state = self[$state$];
  state.bailed = true;
  state.return = (...args) => {
    this[$state$] = {
      plugins: state.plugins,
      startIndex: index,
      destroyed: false,
      triggerGoto: true,
    };
    return self.call(...args);
  };
  const triggerGoto = this.interceptor.trigger.goto;
  if (triggerGoto) {
    triggerGoto(name);
  }
  return true;
}

function iteratorGetState<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>) {
  const state = this[$state$];
  return state.plugins;
}

function iteratorFindIndex<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: HookPluginName) {
  const state = this[$state$];
  const id = serializePluginName(name);
  return state.plugins.findIndex(plugin => serializePluginName(plugin) === id);
}

function iteratorHas<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: HookPluginName) {
  return this.findIndex(name) >= 0;
}

function iteratorEnable<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: HookPluginName, enable = true) {
  const index = this.findIndex(name);
  const state = this[$state$];
  if (index < 0) {
    return false;
  }
  (<any>state)[`disabled${index}`] = !enable;
  const triggerEnable = this.interceptor.trigger.enable;
  if (triggerEnable) {
    triggerEnable(name, enable);
  }
  return true;
}

function iteratorDisable<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: string) {
  return this.enable(name, false);
}

function iteratorStatus<Caller extends CallerFn, Result = void>(this: HookIteratorWithState<Caller, Result>, name: string): boolean | null {
  const state = this[$state$];
  const index = this.findIndex(name);
  if (index < 0) {
    return null;
  }
  const key = `disabled${index}`;
  if (index >= 0 && key in state) {
    return (<any>state)[key];
  }
  return true;
}

export type CallerFn<Params extends [...any[]] = [...any[]], Result = any> = (...args: Params) => Result;

export type CallerWithIterator<Caller extends CallerFn> =
  (iterator: HookIterator<Caller>, ...args: FunctionArguments<Caller>) => ReturnType<Caller>;
export type CallerWithContext<Caller extends CallerFn> =
  (context: HookIteratorContext, ...args: FunctionArguments<Caller>) => ReturnType<Caller>;

export type ArgumentNames<Caller extends CallerFn> =
  FunctionArguments<Caller> extends [any, any, any, any] ? [string, string, string, string]
    : FunctionArguments<Caller> extends [any, any, any] ? [string, string, string]
      : FunctionArguments<Caller> extends [any, any] ? [string, string]
        : FunctionArguments<Caller> extends [any] ? [string]
          : FunctionArguments<Caller> extends [] ? [] : string[];

export type HookEvents<Caller extends (...args: any[]) => any, IterateCallResult = any> = {
  /**
   * When a new plugin is binded
   * @param plugin
   */
  bind(plugin: HookListener<Caller>): void;
  /**
   * When a plugin is unbinded
   * @param plugin
   */
  unbind(plugin: HookListener<Caller>): void;
  /**
   * When hook iteration is created
   * @param iterator
   * @param args
   */
  iterate(iterator: HookIterator<Caller, IterateCallResult>): void;
  /**
   * When hook is called
   * @param iterator
   * @param args
   */
  call(iterator: HookIterator<Caller, IterateCallResult>, ...args: FunctionArguments<Caller>): void;
}

const argVarRegExp = /([a-zA-Z$][a-zA-Z0-9_$]+)/;

export function serializePluginName(name: HookListener | HookPluginName): string {
  if (name instanceof HookListener) {
    return serializePluginName([name.context, name.name]);
  }
  if (Array.isArray(name)) {
    return `${name[0] ? `${JSON.stringify(typeof name[0] === 'string' ? name[0] : name[0].hookContext)}.` : ''}${JSON.stringify(name[1] || 'default')}`;
  }
  if (typeof name !== 'string') {
    return `${JSON.stringify(name.hookContext)}.${JSON.stringify('default')}`;
  }
  return JSON.stringify(name);
}

export abstract class Hook<Caller extends CallerFn = CallerFn, IterateCallResult = any> {

  private [$state$]: {
    pluginMapping: {
      [pluginName: string]: HookListener<Caller>;
    },
    size: number;
    usingOrdering: number,
    pluginOrdering?: HookListener<Caller>[],
    pluginIteration?: (this: HookIterator<Caller, IterateCallResult>, ...args: FunctionArguments<Caller>) => IterateCallResult,
  };

  constructor(readonly argVars?: ArgumentNames<Caller>, readonly defaultPriority = 30) {
    if (argVars) {
      for (const argVar of argVars) {
        if (!argVarRegExp.test(argVar)) {
          throw new Error(`Invalid argument variable name "${argVar}"`);
        }
      }
    }
    this[$state$] = {
      size: 0,
      pluginMapping: {},
      usingOrdering: 0,
    };
  }

  intercept(interceptor: Partial<HookEvents<Caller, IterateCallResult>>) {
    return this.interceptor.intercept(interceptor);
  }

  readonly interceptor = new Interceptor<HookEvents<Caller, IterateCallResult>>();

  protected abstract createIteration(): (this: HookIterator<Caller, IterateCallResult>, ...args: FunctionArguments<Caller>) => IterateCallResult;

  has(plugin: HookListener<Caller>): boolean;
  has(name: HookPluginName): boolean;
  has(pluginOrName: HookPluginName | HookListener<Caller> ) {
    const plugin = this[$state$].pluginMapping[serializePluginName(name)];
    if (plugin == null) {
      return false;
    }
    if (pluginOrName instanceof HookListener) {
      return pluginOrName === plugin;
    }
    return true;
  }

  get(name: HookPluginName) {
    const plugin = this[$state$].pluginMapping[serializePluginName(name)];
    if (plugin) {
      return plugin;
    }
    return null;
  }

  get size() {
    return this[$state$].size;
  }

  bind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default', caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<ListenerCaller extends Caller>(name: HookPluginName, iterate: null, caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller, priority?: number): HookListener<Caller, 'iterator', ListenerCaller>
  bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller, priority?: number): HookListener<Caller, 'iterator', ListenerCaller>
  bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller, priority?: number): HookListener<Caller, 'context', ListenerCaller>
  bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller, priority?: number): HookListener<Caller, 'context', ListenerCaller>
  bind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller, priority?: number): HookListener<Caller, Mode, ListenerCaller>
  bind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<ListenerCaller extends Caller>(name: HookPluginName, iterate: null, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'iterator', ListenerCaller>
  bind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'iterator', ListenerCaller>
  bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'context', ListenerCaller>
  bind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, 'context', ListenerCaller>
  bind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller, order: CallerOrder[] | CallerOrder, priorityFallback?: number): HookListener<Caller, Mode, ListenerCaller>
  bind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller, priority?: number): HookListener<Caller, 'default', ListenerCaller>
  bind<P extends HookListener<Caller>>(plugin: P): P
  bind(name: HookListener<Caller> | HookPluginName, ...rest: any[]): HookListener<Caller> {
    const callerIndex = typeof rest[0] !== 'function' ? 1 : 0;
    const state = this[$state$];
    const plugin = typeof name === 'object'
      && !Array.isArray(name)
      && name instanceof HookListener
        ? name
        : new HookListener(
          name,
          this,
          <any>(rest[0] === true ? 'iterator' : (
            rest[0] === false ? 'context' : (
              typeof rest[0] === 'string' ? rest[0] : 'default'
            )
          )),
          rest[callerIndex],
          typeof rest[callerIndex + 1] === 'number' || rest[callerIndex + 1] == null ? undefined : makeArray(rest[callerIndex + 1]),
          typeof rest[callerIndex + 1] === 'number' ? rest[callerIndex + 1] : rest[callerIndex + 2],
        );
    if (plugin.hook !== this) {
      throw new Error(`Owned by another hook`);
    }
    const id = serializePluginName(name);
    if (state.pluginMapping[id] === plugin) {
      return plugin;
    }
    state.pluginMapping[id] = plugin;
    if (plugin.priority || (plugin.order && plugin.order.length)) {
      this[$state$].usingOrdering += 1;
    }
    delete state.pluginOrdering;
    delete state.pluginIteration;
    state.size += 1;
    const bindTrigger = this.interceptor.trigger.bind;
    if (bindTrigger) {
      bindTrigger(plugin);
    }
    const pluginBindTrigger = plugin.interceptor.trigger.bind;
    if (pluginBindTrigger) {
      pluginBindTrigger(this);
    }
    return plugin;
  }

  unbind(name: HookPluginName): HookListener<Caller> | null
  unbind<ListenerCaller extends Caller>(name: HookPluginName, caller: ListenerCaller): HookListener<Caller, 'default', ListenerCaller> | null
  unbind<ListenerCaller extends Caller>(name: HookPluginName, mode: 'default' | null, caller: ListenerCaller): HookListener<Caller, 'default', ListenerCaller> | null
  unbind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, mode: 'iterator', caller: ListenerCaller): HookListener<Caller, 'iterator', ListenerCaller> | null
  unbind<ListenerCaller extends CallerWithIterator<Caller>>(name: HookPluginName, iterate: true, caller: ListenerCaller): HookListener<Caller, 'iterator', ListenerCaller> | null
  unbind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, mode: 'context', caller: ListenerCaller): HookListener<Caller, 'context', ListenerCaller> | null
  unbind<ListenerCaller extends CallerWithContext<Caller>>(name: HookPluginName, iterate: false, caller: ListenerCaller): HookListener<Caller, 'context', ListenerCaller> | null
  unbind<Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>>(name: HookPluginName, mode: Mode, caller: ListenerCaller): HookListener<Caller, Mode, ListenerCaller> | null
  unbind<P extends HookListener<Caller>>(plugin: P): P | null
  unbind(name: HookListener<Caller> | HookPluginName, ...rest: any[]): HookListener<Caller> | null {
    const state = this[$state$];
    const id = serializePluginName(name);
    const plugin = state.pluginMapping[id];
    if (name instanceof HookListener && plugin !== name) {
      return null;
    }
    if (plugin != null) {
      if (
        rest.length === 0
        || (
          (typeof rest[0] === 'function' || (rest[0] === true ? 'iterator' : (rest[0] === false ? 'context' : rest[0])) === plugin.mode)
          && ((typeof rest[0] === 'function' || rest.length > 1) ? (plugin.caller === rest[rest.length > 1 ? 1 : 0]) : true)
        )
      ) {
        delete state.pluginMapping[id];
        if (plugin.priority || (plugin.order && plugin.order.length)) {
          state.usingOrdering += -1;
        }
        delete state.pluginOrdering;
        delete state.pluginIteration;
        state.size += -1;
        const unbindTrigger = this.interceptor.trigger.unbind;
        if (unbindTrigger) {
          unbindTrigger(plugin);
        }
        const pluginUnbindTrigger = plugin.interceptor.trigger.unbind;
        if (pluginUnbindTrigger) {
          pluginUnbindTrigger(this);
        }
        return <any>plugin;
      }
    }
    return null;
  }

  plugins() {
    const state = this[$state$];
    let plugins = state.pluginOrdering;
    if (plugins != null) {
      return plugins.slice(0);
    }
    plugins = [];
    const mappingPlugins = this[$state$].pluginMapping;
    if (mappingPlugins != null) {
      if (this[$state$].usingOrdering < 1) {
        for (const pluginName of Object.keys(mappingPlugins)) {
          plugins.push(mappingPlugins[pluginName]);
        }
      } else {
        const order: {
          [priority: string]: HookListener<Caller>[]
        } = {};
        let before: {
          [pluginId: string]: HookListener<Caller>[];
        } | undefined;
        let after: {
          [pluginId: string]: HookListener<Caller>[];
        } | undefined;
        for (const pluginId of Object.keys(mappingPlugins)) {
          const plugin = mappingPlugins[pluginId];
          let matchOrder = false;
          if (plugin.order != null) {
            for (const match of plugin.order) {
              if ('before' in match) {
                const id = serializePluginName(match.before);
                if (mappingPlugins[id] != null) {
                  if (before == null) {
                    before = {};
                  }
                  if (before[id] == null) {
                    before[id] = [];
                  }
                  before[id].push(plugin);
                  matchOrder = true;
                  break;
                }
              } else if ('after' in match) {
                const id = serializePluginName(match.after);
                if (mappingPlugins[id] != null) {
                  if (after == null) {
                    after = {};
                  }
                  if (after[id] == null) {
                    after[id] = [];
                  }
                  after[id].push(plugin);
                  matchOrder = true;
                  break;
                }
              }
            }
          }
          const priority = plugin.priority || this.defaultPriority;
          if (!matchOrder) {
            if (order[priority] == null) {
              order[priority] = [];
            }
            order[priority].push(plugin);
          }
        }
        for (const priority of Object.keys(order).sort(sortNumbers)) {
          for (const plugin of order[priority]) {
            const id = serializePluginName(plugin);
            const beforePlugins = before != null && before[id] != null
              ? before[id]
              : undefined;
            const afterPlugins = after != null && after[id] != null
              ? after[id]
              : undefined;
            if (beforePlugins != null) {
              for (const plugin of beforePlugins) {
                plugins.push(plugin);
              }
            }
            plugins.push(plugin);
            if (afterPlugins != null) {
              for (const plugin of afterPlugins) {
                plugins.push(plugin);
              }
            }
          }
        }
      }
    }
    state.pluginOrdering = plugins;
    return plugins;
  }

  iterate(context?: HookIteratorContext): HookIterator<Caller, IterateCallResult> {
    const state = this[$state$];
    let call = state.pluginIteration;
    if (!call) {
      state.pluginIteration = call = this.createIteration();
    }
    let iteratorContext: HookIteratorContext;
    let passingContext = false;
    if (context) {
      iteratorContext = context;
      passingContext = true;
    } else {
      iteratorContext = {};
    }
    const interceptor = new Interceptor<HookIteratorEvents<Caller, IterateCallResult>>();
    const iterator = {
      call,
      passingContext,
      interceptor,
      intercept: interceptor.intercept,
      context: iteratorContext,
      bail: iteratorBail,
      goto: iteratorGoto,
      enable: iteratorEnable,
      disable: iteratorDisable,
      status: iteratorStatus,
      findIndex: iteratorFindIndex,
      getState: iteratorGetState,
      has: iteratorHas,
      hook: this,
      [$state$]: {
        plugins: state.pluginOrdering,
        startIndex: 0,
        destroyed: false,
      },
    };
    const triggerIterate = this.interceptor.trigger.iterate;
    if (triggerIterate) {
      triggerIterate(iterator);
    }
    return iterator;
  }

}


export type CallerMode = 'context' | 'iterator' | 'default';
export type CallerOrder = { before: HookPluginName } | { after: HookPluginName };

export type CallerWithMode<Mode extends CallerMode, Caller extends CallerFn> = Mode extends 'iterator'
  ? CallerWithIterator<Caller>
  : (Mode extends 'context' ? CallerWithContext<Caller> : Caller);

export type HookListenerEvents<Caller extends CallerFn, Mode extends CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller>> = {
  bind(hook: Hook): void;
  unbind(hook: Hook): void;
  enable(state: boolean): void;
}

export type HookPluginName = string | HookPlugin | [string | HookPlugin] | [undefined | string | HookPlugin, string];

export class HookListener<Caller extends CallerFn = CallerFn, Mode extends CallerMode = CallerMode, ListenerCaller extends CallerWithMode<Mode, Caller> = CallerWithMode<Mode, Caller>> {

  readonly disabled: boolean;

  readonly name: string;
  readonly contextValue?: HookPlugin | string;
  readonly context?: string;

  constructor(
    name: HookPluginName,
    readonly hook: Hook<Caller>,
    readonly mode: Mode,
    readonly caller: ListenerCaller,
    readonly order?: CallerOrder[],
    readonly priority?: number,
  ) {
    if (Array.isArray(name)) {
      const contextValue = name[0];
      this.contextValue = contextValue;
      this.context = contextValue == null
        ? contextValue
        : (typeof contextValue === 'string'
            ? contextValue
            : contextValue.hookContext
          );
      this.name = name[1] || 'default';
    } else if (typeof name !== 'string') {
      this.contextValue = name;
      this.context = name.hookContext;
      this.name = 'default';
    } else {
      this.name = name;
    }
  }

  intercept(interceptor: Partial<HookListenerEvents<Caller, Mode, ListenerCaller>>) {
    return this.interceptor.intercept(interceptor);
  }

  readonly interceptor = new Interceptor<HookListenerEvents<Caller, Mode, ListenerCaller>>();

  callerLength: number | undefined;

  get binded() {
    return this.hook.has(this);
  }

  bind() {
    return this.hook.bind(this);
  }

  unbind() {
    return this.hook.unbind(this);
  }

  /**
   * Usable when resting arguments
   * @param length
   */
  args(length: number) {
    this.callerLength = length;
    return this;
  }

  enable(state = true) {
    (<any>this).disabled = !state;
    const enableTrigger = this.interceptor.trigger.enable;
    if (enableTrigger) {
      enableTrigger(state);
    }
    return this;
  }

  disable() {
    return this.enable(false);
  }

  toString() {
    return `[HookListener ${serializePluginName(this)}]`;
  }

}

export interface HookPlugin {
  hookContext: string;
}