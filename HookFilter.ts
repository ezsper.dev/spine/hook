import {
  Hook,
  CallerFn,
  Promiseable,
  FunctionArguments,
  forgeArgumentNames,
  HookIteratorContext,
} from './Hook';
import {
  createFilterIterator,
  FilterIterator,
} from './factory/filter';

export * from './Hook';

export type FilterCallerFn<T> = CallerFn<[T, ...any[]], Promiseable<T>>;

export class HookFilter<T = any, Caller extends FilterCallerFn<T> = FilterCallerFn<T>> extends Hook<Caller, Promise<T>> {

  static template<T, Caller extends FilterCallerFn<T>>(caller: FilterCallerFn<T> & Caller): HookFilter<T, Caller> {
    return new HookFilter(forgeArgumentNames(caller));
  }

  addFilter = this.bind;
  removeFilter = this.unbind;

  createIteration(): FilterIterator<T, Caller> {
    return createFilterIterator<T, Caller>(this);
  }

  filter(...args: FunctionArguments<Caller>) {
    return this.iterate().call(...args);
  }

  filterPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>) {
    return this.iterate(context).call(...args);
  }

}