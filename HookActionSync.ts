import {
  Hook,
  CallerFn,
  HookIteratorContext,
  FunctionArguments,
  forgeArgumentNames,
} from './Hook';
import {
  createActionSyncIterator,
  ActionSyncIterator,
} from './factory/actionSync';

export * from './Hook';

export class HookActionSync<Caller extends CallerFn = CallerFn> extends Hook<Caller, void> {

  static template<Caller extends CallerFn>(caller: Caller): HookActionSync<Caller> {
    return new HookActionSync(forgeArgumentNames(caller));
  }

  addAction = this.bind;
  removeAction = this.unbind;

  protected createIteration(): ActionSyncIterator<Caller> {
    return createActionSyncIterator<Caller>(this);
  }

  do(...args: FunctionArguments<Caller>) {
    return this.iterate().call(...args);
  }

  doPassing(context: HookIteratorContext, ...args: FunctionArguments<Caller>) {
    return this.iterate(context).call(...args);
  }

}