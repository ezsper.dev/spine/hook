/// <reference types="jest" />
import {
  HookFilterSync,
  HookFilter,
} from '../../..';

describe('Filters', () => {

  it('HookFilterSync', () => {
    const hook = HookFilterSync.template((value: number, factor: number) => value);
    hook.addFilter('multiply', (value, factor) => value * factor); // 2
    hook.addFilter('divide', (value, factor) => value / 4); // 4
    hook.addFilter('sum', (value, factor) => value + 8, { after: 'multiply' }); // 3
    hook.addFilter('subtract', (value, factor) => value - 1, { before: 'multiply' }); // 1
    const value = hook.filter(3, 4);
    expect(value).toBe(4);
  });

  it('HookFilter', async () => {
    const myPlugin = { hookContext: 'myPlugin' };
    const hook = HookFilter.template((value: number, factor: number) => value);
    hook.addFilter([myPlugin, 'multiply'], (value, factor) => value * factor); // 2
    hook.addFilter('divide', (value, factor) => value / 4); // 4
    hook.addFilter('sum', (value, factor) => value + 8, { after: [myPlugin, 'multiply'] }); // 3
    hook.addFilter('subtract', (value, factor) => value - 1,{ before: [myPlugin, 'multiply'] }); // 1
    const value = await hook.filter(3, 4);
    expect(value).toBe(4);
  });

});
