/// <reference types="jest" />
import {
  HookActionSync,
  HookActionSeries,
} from '../../..';

describe('Actions', () => {

  it('HookActionSync', () => {
    const hook = HookActionSync.template((value: number, factor: number) => {});
    hook.addAction('multiply', false,(context, value, factor) => {
      context.multiply = value * factor;
    }); // 2
    hook.addAction('divide', false, (context, value, factor) => {
      context.divide = value / 4;
    }); // 4
    hook.addAction('sum', false,(context, value, factor) => {
      context.sum = value + 8;
    }, { after: 'multiply' }); // 3
    hook.addAction('subtract', false, (context, value, factor) => {
      context.subtract = value - 1;
    }, { before: 'multiply' }); // 1
    const context: any = {};
    hook.doPassing(context, 3, 4);
    const match = {
      subtract: 2,
      multiply: 12,
      sum: 11,
      divide: 0.75,
    };
    expect(Object.keys(context)).toEqual(Object.keys(match));
    expect(context).toEqual(match);
  });

  it('HookAction', async () => {
    const hook = HookActionSeries.template((value: number, factor: number) => {});
    hook.addAction('multiply', false,(context, value, factor) => {
      context.multiply = value * factor;
    }); // 2
    hook.addAction('divide', false, (context, value, factor) => {
      context.divide = value / 4;
    }); // 4
    hook.addAction('sum', false, (context, value, factor) => {
      context.sum = value + 8;
    }, { after: 'multiply' }); // 3
    hook.addAction('subtract', false, (context, value, factor) => {
      context.subtract = value - 1;
    }, { before: 'multiply' }); // 1
    const context: any = {};
    await hook.doPassing(context,3, 4);
    const match = {
      subtract: 2,
      multiply: 12,
      sum: 11,
      divide: 0.75,
    };
    expect(Object.keys(context)).toEqual(Object.keys(match));
    expect(context).toEqual(match);
    expect(hook.size).toBe(4);
  });

});
