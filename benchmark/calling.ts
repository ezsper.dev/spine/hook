import { Suite } from 'benchmark';
import * as tapable from 'tapable';
import * as spine from '..';

const suite = new Suite();

const tapableHook = new tapable.SyncHook<number>(['speed']);

tapableHook.tap('plugin1', (speed) => {
  const math = speed * 10;
});
tapableHook.tap('plugin2', (speed) => {
  const math = speed * 10;
});
tapableHook.tap('plugin3', (speed) => {
  const math = speed * 10;
});
tapableHook.tap('plugin4', (speed) => {
  const math = speed * 10;
});

const spineActionSyncHook = spine.HookActionSync.template((speed: number) => {});

spineActionSyncHook.addAction('plugin1', (speed) => {
  const math = speed * 10;
});
spineActionSyncHook.addAction('plugin2', (speed) => {
  const math = speed * 10;
});
spineActionSyncHook.addAction('plugin3', (speed) => {
  const math = speed * 10;
});
spineActionSyncHook.addAction('plugin4', (speed) => {
  const math = speed * 10;
});

export default function benchmark() {
  console.log("Calling hook");
  return suite
    .add('Tapable (SyncHook.call)', () => {
      tapableHook.call(10);
    })
    .add('Spine (HookActionSync.do)', () => {
      spineActionSyncHook.do(10);
    });
}