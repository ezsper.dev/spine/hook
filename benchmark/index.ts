import instantiate from './instantiate';
import binding from './binding';
import calling from './calling';

const benchmarks = [calling, binding, instantiate];

(async () => {
  for (const benchmark of benchmarks) {
    console.log('');
    const suite = benchmark()
      .on('cycle', (event: any) => {
        console.log(String(event.target));
      })
      .on('complete', () => {
        console.log(`Fastest is "${suite.filter('fastest').map(<any>'name')}"`);
      });
    await suite.run();
  }
})();