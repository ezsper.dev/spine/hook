import { Suite } from 'benchmark';
import * as tapable from 'tapable';
import * as spine from '..';

const suite = new Suite();

export default function benchmark() {
  console.log("Creating instance...");
  return suite
    .add('Tapable (SyncHook)', () => {
      const hook = new tapable.SyncHook<number>(['speed']);
    })
    .add('Spine (HookActionSync)', () => {
      const hook = new spine.HookActionSync<[number]>(['speed']);
    });
}