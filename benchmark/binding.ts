import { Suite } from 'benchmark';
import * as tapable from 'tapable';
import * as spine from '..';

const suite = new Suite();

const tapableHook = new tapable.SyncHook<number>(['speed']);
const spineActionSyncHook = spine.HookActionSync.template((speed: number) => {});

export default function benchmark() {
  console.log("Tapping vs Binding");
  return suite
    .add('Tapable (SyncHook.tap)', () => {
      tapableHook.tap('plugin1', (speed) => {
        const math = speed * 10;
      });
      tapableHook.tap('plugin2', (speed) => {
        const math = speed * 10;
      });
      tapableHook.tap('plugin3', (speed) => {
        const math = speed * 10;
      });
      tapableHook.tap('plugin4', (speed) => {
        const math = speed * 10;
      });
    })
    .add('Spine (HookActionSync.addAction)', () => {
      spineActionSyncHook.addAction('plugin1', (speed) => {
        const math = speed * 10;
      });
      spineActionSyncHook.addAction('plugin2', (speed) => {
        const math = speed * 10;
      });
      spineActionSyncHook.addAction('plugin3', (speed) => {
        const math = speed * 10;
      });
      spineActionSyncHook.addAction('plugin4', (speed) => {
        const math = speed * 10;
      });
    });
}